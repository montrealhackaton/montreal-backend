(function () {
  'use strict';

  angular
      .module('montrealApp')
      .controller('ProdutoController', ProdutoController);

  ProdutoController.$inject = ['$http'];

  function ProdutoController($http) {
    var vm = this;

    vm.novoHabilitado = false;
    vm.buscarLista = buscarLista;
    vm.selecionar = selecionar;
    vm.incluir = incluir;
    vm.salvar = salvar;
    vm.incluirRelacionado = incluirRelacionado;
    vm.salvarRelacionado = salvarRelacionado;
    vm.ouvir = ouvir;

    iniciar();

    function iniciar() {
      vm.buscarLista();
    }

    function buscarLista() {
      $http.get('http://localhost:1337/produto').then(function (resposta) {
        vm.produtos = resposta.data;
        vm.incluir();
      });
    }

    function selecionar(indice) {
      vm.produto = vm.produtos[indice];
      vm.novoHabilitado = true;

      $http.get('http://localhost:1337/relacionado?id_produto_base=' + vm.produto.id).then(function (resposta) {
        vm.relacionados = resposta.data;
      });
    }

    function incluir() {
      vm.produto = {};
      vm.relacionados = {};
      vm.novoHabilitado = false;
    }

    function salvar() {
      var metodo = 'POST',
          complementoURL = '';

      if (angular.isDefined(vm.produto.id)) {
        metodo = 'PUT';
        complementoURL = '/' + vm.produto.id;
      }

      $http({
        method: metodo,
        url: 'http://localhost:1337/produto' + complementoURL,
        data: vm.produto
      }).then(function (resposta) {
        vm.buscarLista();
      });
    }

    function incluirRelacionado() {
      if (angular.isUndefined(vm.relacionados)) {
        vm.relacionados = [];
      }

      vm.relacionados.push({
        id_produto_base: vm.produto.id
      });
    }

    function salvarRelacionado() {
      angular.forEach(vm.relacionados, function (item) {
        if (angular.isDefined(item.id_produto_relacionado)) {
          $http.post('http://localhost:1337/relacionado', {
            method: 'POST',
            data: item
          });
        }
      });
    }

    function ouvir(atributo) {

    }
  }
})();