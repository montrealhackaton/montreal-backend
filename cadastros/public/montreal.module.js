(function() {
  'use strict';

  angular
      .module('montrealApp', ['ui.router',
                              'monospaced.qrcode',
                              'io-barcode'])
      .config(configurar);

  configurar.$inject = ['$stateProvider', '$urlRouterProvider'];

  function configurar($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/produto");
    //
    // Now set up the states
    $stateProvider
        .state('produto', {
          url: '/produto',
          templateUrl: 'produto/produto.html',
          controller: 'ProdutoController',
          controllerAs: 'vm'
        });
  }
})();