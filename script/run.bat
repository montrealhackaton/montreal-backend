@echo off

set server=%1
set database=%2
set username=%3
set password=%4

for %%a in (%~dp0structure\*.sql %~dp0programmability\*.sql) do (
  @echo Running %%a
  sqlcmd /S %server% /d %database% -U %username% -P %password% -i"%%a"
)