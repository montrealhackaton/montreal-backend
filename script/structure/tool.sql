if not exists(select sch.schema_id
                from sys.schemas sch
               where sch.name = 'tool')
begin
  exec('create schema tool');
end;