exec('create schema vitrine');
go

if object_id('vitrine.loja', 'U') is null
begin
  create table vitrine.loja(
    id varchar(36) not null,
    nome_fantasia varchar(200) not null,
    constraint pkyLoja primary key(id)
  );
end;
go

if object_id('vitrine.produto', 'U') is null
begin
  create table vitrine.produto(
    id varchar(36) not null,
    id_loja varchar(36) not null,
    nome varchar(200) not null,
    descricao varchar(max),
    codigo_barra varchar(255),
    localizacao varchar(100),
    constraint pkyProduto primary key(id),
    constraint fkyProdutoLoja foreign key(id_loja)
    references vitrine.loja(id) on update cascade on delete cascade
  );
end;
go

if object_id('vitrine.relacionado', 'U') is null
begin
  create table vitrine.relacionado(
    id_produto_base varchar(36) not null,
    id_produto_relacionado varchar(36) not null,
    constraint pkyRelacionado primary key(id_produto_base, id_produto_relacionado),
    constraint fkyRelacionadoBase foreign key(id_produto_base)
    references vitrine.produto(id),
    constraint fkyRelacionadoRelacionado foreign key(id_produto_base)
    references vitrine.produto(id)
  );
end;
go