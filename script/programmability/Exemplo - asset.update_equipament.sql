if object_id('asset.update_equipament', 'P') is null
begin
  exec('create procedure asset.update_equipament as');
end;
go

alter procedure asset.update_equipament @id          int,
                                        @acquisition datetime
as
begin
  set nocount on;

  update equip
     set equip.acquisition = @acquisition
    from asset.equipament equip
   where equip.id = @id;
end;
go