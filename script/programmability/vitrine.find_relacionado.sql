if object_id('vitrine.find_relacionado', 'P') is null
begin
  exec('create procedure vitrine.find_relacionado as');
end;
go

alter procedure vitrine.find_relacionado @id_produto_base varchar(36)
as
begin
  set nocount on;

  select rel.id_produto_base,
         rel.id_produto_relacionado,
         prod.nome,
         prod.localizacao
    from vitrine.relacionado rel with(readpast)
         inner join vitrine.produto prod with(readpast) on prod.id = rel.id_produto_relacionado
   where rel.id_produto_base = @id_produto_base
   order by prod.nome;
end;
go
