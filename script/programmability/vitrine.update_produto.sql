if object_id('vitrine.update_produto', 'P') is null
begin
  exec('create procedure vitrine.update_produto as');
end;
go

alter procedure vitrine.update_produto @id           varchar(36),
                                       @id_loja      varchar(36),
                                       @nome         varchar(200),
                                       @descricao    varchar(max),
                                       @codigo_barra varchar(255),
                                       @localizacao  varchar(100)
as
begin
  set nocount on;

  update prod
     set prod.id_loja = @id_loja,
         prod.nome = @nome,
         prod.descricao = @descricao,
         prod.codigo_barra = @codigo_barra,
         prod.localizacao = @localizacao
    from vitrine.produto prod
   where prod.id = @id;
end;
go
