if object_id('asset.create_equipament', 'P') is null
begin
  exec('create procedure asset.create_equipament as');
end;
go

alter procedure asset.create_equipament @acquisition datetime
as
begin
  declare @identity table(id int);
  declare @id int;

  set nocount on;

  -- Insert the new registry and return the identity field created to a table
  insert into asset.equipament(acquisition)
  output inserted.id into @identity
  values(@acquisition);

  -- Fill the identity in the variable
  select @id = id.id
    from @identity id;

  -- Return the identity
  return @id;
end;
go