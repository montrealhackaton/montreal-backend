if object_id('asset.find_equipament', 'P') is null
begin
  exec('create procedure asset.find_equipament as');
end;
go

alter procedure asset.find_equipament @id          int      = null,
                                      @acquisition datetime = null
as
begin
  set nocount on;

  select equip.id,
         equip.acquisition
    from asset.equipament equip with (readpast)
   where (@id is null or equip.id = @id)
     and (@acquisition is null or equip.acquisition = @acquisition);
end;
go
