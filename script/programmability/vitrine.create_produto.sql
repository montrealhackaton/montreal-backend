if object_id('vitrine.create_produto', 'P') is null
begin
  exec('create procedure vitrine.create_produto as');
end;
go

alter procedure vitrine.create_produto @id_loja      varchar(36),
                                       @nome         varchar(200),
                                       @descricao    varchar(max),
                                       @codigo_barra varchar(255),
                                       @localizacao  varchar(100)
as
begin
  set nocount on;

  insert vitrine.produto(id,
                         id_loja,
                         nome,
                         descricao,
                         codigo_barra,
                         localizacao)
                  values(newid(),
                         @id_loja,
                         @nome,
                         @descricao,
                         @codigo_barra,
                         @localizacao);
end;
go
