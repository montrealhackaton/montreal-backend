if object_id('tool.find_one', 'P') is null
begin
  exec('create procedure tool.find_one as');
end;
go

alter procedure tool.find_one @schema sysname = 'dbo',
                              @table  sysname,
                              @id     varchar(max)
as
begin
  declare @vColumns table(name       sysname,
                          is_primary bit,
                          type       sysname);
  declare @vQuery varchar(max);

  set nocount on;

  set @schema = isnull(@schema, 'dbo');

  insert into @vColumns(name,
                        is_primary,
                        type)
  select col.name,
         case kc.type
           when 'PK' then 1
           else 0
         end,
         typ.name
    from sys.schemas sch with(readpast)
   inner join sys.tables tbl with(readpast)
      on tbl.schema_id = sch.schema_id
   inner join sys.columns col with(readpast)
      on col.object_id = tbl.object_id
   inner join sys.types typ with(readpast)
      on typ.user_type_id = col.user_type_id
    left join sys.index_columns ico with(readpast)
      on ico.object_id = tbl.object_id
     and ico.column_id = col.column_id
    left join sys.key_constraints kc with(readpast)
      on kc.parent_object_id = ico.object_id
   where sch.name = @schema
     and tbl.name = @table;

  if not exists(select col.name
                  from @vColumns col
                 where col.is_primary = 1)
  begin
    raiserror('No primary key found.', 16, 1);
    return;
  end;

  if (select count(col.name)
        from @vColumns col
       where col.is_primary = 1) > 1
  begin
    raiserror('More than one column found on primary key.', 16, 1);
    return;
  end;

  select @vQuery = isnull(@vQuery + ',' + char(10) + '       ', '') + col.name
    from @vColumns col;

  select @vQuery = 'select ' + @vQuery + char(10)
                    + ' from [' + @schema + '].[' + @table + '] with(readpast)' + char(10)
                    + ' where '
                    + case col.type
                        when 'varchar' then col.name
                        else 'cast(' + col.name + ' as varchar)'
                      end
                    + ' = ''' + @id + ''''
    from @vColumns col
   where col.is_primary = 1;

  exec(@vQuery);

  if @@rowcount = 0
  begin
    raiserror('Record not found.', 16, 1);
    return;
  end
  else if @@rowcount > 1
  begin
    raiserror('More than one record found.', 16, 1);
    return;
  end;
end;
go
