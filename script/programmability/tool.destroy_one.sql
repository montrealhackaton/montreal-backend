if object_id('tool.destroy_one', 'P') is null
begin
  exec('create procedure tool.destroy_one as');
end;
go

alter procedure tool.destroy_one @schema sysname,
                                 @table  sysname,
                                 @id     varchar(max)
as
begin
  declare @vColumns table(name sysname,
                          type sysname);
  declare @vQuery varchar(max);

  set nocount on;

  set @schema = isnull(@schema, 'dbo');

  insert into @vColumns(name,
                        type)
  select col.name,
         typ.name
    from sys.schemas sch with(readpast)
   inner join sys.tables tbl with(readpast)
      on tbl.schema_id = sch.schema_id
   inner join sys.columns col with(readpast)
      on col.object_id = tbl.object_id
   inner join sys.types typ with(readpast)
      on typ.user_type_id = col.user_type_id
    left join sys.index_columns ico with(readpast)
      on ico.object_id = tbl.object_id
     and ico.column_id = col.column_id
    left join sys.key_constraints kc with(readpast)
      on kc.parent_object_id = ico.object_id
   where sch.name = @schema
     and tbl.name = @table
     and kc.type = 'PK';

  if not exists(select col.name
                  from @vColumns col)
  begin
    raiserror('No primary key found.', 16, 1);
    return;
  end;

  if (select count(col.name)
        from @vColumns col) > 1
  begin
    raiserror('More than one column found on primary key.', 16, 1);
    return;
  end;

  select @vQuery = 'delete [' + @schema + '].[' + @table + ']' + char(10)
                    + ' where '
                    + case col.type
                        when 'varchar' then col.name
                        else 'cast(' + col.name + ' as varchar)'
                      end
                    + ' = ''' + @id + ''''
    from @vColumns col;

  exec(@vQuery);

  if @@rowcount = 0
  begin
    raiserror('Record not found.', 16, 1);
    return;
  end
  else if @@rowcount > 1
  begin
    raiserror('More than one record found.', 16, 1);
    return;
  end;
end;
go
