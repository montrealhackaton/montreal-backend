if object_id('vitrine.find_produto', 'P') is null
begin
  exec('create procedure vitrine.find_produto as');
end;
go

alter procedure vitrine.find_produto @id_loja varchar(36) = null,
                                     @codigo_barra varchar(255) = null
as
begin
  set nocount on;

  select prod.id,
         prod.id_loja,
         prod.nome,
         prod.descricao,
         prod.codigo_barra,
         prod.localizacao
    from vitrine.produto prod with(readpast)
   where (@id_loja is null or prod.id_loja = @id_loja)
     and (@codigo_barra is null or prod.codigo_barra = @codigo_barra);
end;
go
