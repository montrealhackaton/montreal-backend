(function() {
  'use strict';

  /**
   * Module dependencies
   */

  var actionUtil = require('../../node_modules/sails/lib/hooks/blueprints/actionUtil');

  /**
   * Update One Record
   *
   * An API call to update a model instance with the specified `id`,
   * treating the other unbound parameters as attributes.
   *
   * @param {Integer|String} id  - the unique id of the particular record you'd like to update  (Note: this param should be specified even if primary key is not `id`!!)
   * @param *                    - values to set on the record
   *
   */
  module.exports = function updateOneRecord (req, res) {
    var Model = actionUtil.parseModel(req),
      queryParameter = "",
      schema,
      query,
      parameters;

    schema = Model.schemaName ? Model.schemaName + '.' : '';
    query = 'exec ' + schema + 'update_' + Model.tableName + ' ';
    parameters = actionUtil.parseValues(req);

    for (var parameterName in parameters) {
      queryParameter = queryParameter
        ? queryParameter + ", "
        : queryParameter;
      queryParameter = queryParameter + "@" + parameterName + " = " + "'" + parameters[parameterName] + "'";
    }

    query = query + queryParameter;

    Model.query(query, function resolveResult(err, results) {
      if (err) {
        return res.serverError(err);
      }

      return res.ok(results);
    });
  };
})();
