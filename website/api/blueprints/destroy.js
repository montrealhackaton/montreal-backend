(function() {
  'use strict';

  /**
   * Module dependencies
   */
  var actionUtil = require('../../node_modules/sails/lib/hooks/blueprints/actionUtil');

  /**
   * Destroy One Record
   *
   * delete  /:modelIdentity/:id
   *    *    /:modelIdentity/destroy/:id
   *
   * Destroys the single model instance with the specified `id` from
   * the data adapter for the given model if it exists.
   *
   * Required:
   * @param {Integer|String} id  - the unique id of the particular instance you'd like to delete
   *
   * Optional:
   * @param {String} callback - default jsonp callback param (i.e. the name of the js function returned)
   */
  module.exports = destroyOneRecord;

  function destroyOneRecord(req, res) {
    var Model = actionUtil.parseModel(req),
      id = actionUtil.requirePk(req),
      query;

    query = 'exec [tool].[destroy_one] ';
    query = Model.schemaName ? query + "@schema = '" + Model.schemaName + "', " : query;
    query = query + "@table = '" + Model.tableName + "', "
      + "@id = '" + id + "'";

    Model.query(query, function(err, results) {
      var result = {};

      if (err) {
        return res.serverError(err);
      }

      if (results.length > 0) {
        result = results[0];
      }

      return res.ok(result);
    });
  };
})();
