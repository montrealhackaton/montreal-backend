(function() {
  'use strict';

  /**
   * Module dependencies
   */
  var actionUtil = require('../../node_modules/sails/lib/hooks/blueprints/actionUtil');

  /**
   * Find Records
   *
   *  get   /:modelIdentity
   *   *    /:modelIdentity/find
   *
   * An API call to find and return model instances from the data adapter
   * using the specified criteria. If an id was specified, just the instance
   * with that unique id will be returned.
   *
   * Optional:
   * @param {Object} where       - the find criteria (passed directly to the ORM)
   * @param {Integer} limit      - the maximum number of records to send back (useful for pagination)
   * @param {Integer} skip       - the number of records to skip (useful for pagination)
   * @param {String} sort        - the order of returned records, e.g. `name ASC` or `age DESC`
   * @param {String} callback - default jsonp callback param (i.e. the name of the js function returned)
   */

  module.exports = findRecords;

  function findRecords(req, res) {
    var Model = actionUtil.parseModel(req),
        queryParameter = "",
        schema,
        query,
        parameters;

    schema = Model.schemaName ? Model.schemaName + '.' : '';
    query = 'exec ' + schema + 'find_' + Model.tableName + ' ';
    parameters = actionUtil.parseCriteria(req);

    for (var parameterName in parameters) {
      queryParameter = queryParameter
        ? queryParameter + ", "
        : queryParameter;
      queryParameter = queryParameter + "@" + parameterName + " = " + "'" + parameters[parameterName] + "'";
    }

    query = query + queryParameter;

    Model.query(query, function resolveResult(err, results) {
      if (err) {
        return res.serverError(err);
      }

      return res.ok(results);
    });
  }
})();