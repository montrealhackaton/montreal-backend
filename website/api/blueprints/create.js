(function() {
  'use strict';

  /**
   * Module dependencies
   */

  var actionUtil = require('../../node_modules/sails/lib/hooks/blueprints/actionUtil');

  /**
   * Create Record
   *
   * post /:modelIdentity
   *
   * An API call to find and return a single model instance from the data adapter
   * using the specified criteria.  If an id was specified, just the instance with
   * that unique id will be returned.
   *
   * Optional:
   * @param {String} callback - default jsonp callback param (i.e. the name of the js function returned)
   * @param {*} * - other params will be used as `values` in the create
   */
  module.exports = function createRecord (req, res) {
    var Model = actionUtil.parseModel(req),
      queryParameter = "",
      schema,
      query,
      parameters;

    schema = Model.schemaName ? Model.schemaName + '.' : '';
    query = 'exec ' + schema + 'create_' + Model.tableName + ' ';
    parameters = actionUtil.parseValues(req);

    for (var parameterName in parameters) {
      queryParameter = queryParameter
        ? queryParameter + ", "
        : queryParameter;

      if (parameters[parameterName].split) {
        queryParameter = queryParameter + "@" + parameterName + " = " + "'" + parameters[parameterName].split("'").join("''") + "'";
      } else  {
        queryParameter = queryParameter + "@" + parameterName + " = " + "'" + parameters[parameterName] + "'";
      }
    }

    query = query + queryParameter;

    Model.query(query, function resolveResult(err, results) {
      if (err) {
        return res.serverError(err);
      }

      return res.ok(results);
    });
  };
})();
