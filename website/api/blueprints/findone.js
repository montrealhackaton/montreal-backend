(function() {
  'use strict';

  /**
   * Module dependencies
   */
  var actionUtil = require('../../node_modules/sails/lib/hooks/blueprints/actionUtil');

  /**
   * Find One Record
   *
   * get /:modelIdentity/:id
   *
   * An API call to find and return a single model instance from the data adapter
   * using the specified id.
   *
   * Required:
   * @param {Integer|String} id - the unique id of the particular instance you'd like to look up *
   *
   * Optional:
   * @param {String} callback - default jsonp callback param (i.e. the name of the js function returned)
   */

  module.exports = findOneRecord;

  function findOneRecord(req, res) {
    var Model = actionUtil.parseModel(req),
      id = actionUtil.requirePk(req),
      query;

    query = 'exec [tool].[find_one] ';
    query = Model.schemaName
      ? query + "@schema = '" + Model.schemaName + "', "
      : query;
    query = query + "@table = '" + Model.tableName + "', "
      + "@id = '" + id + "'";

    Model.query(query, function resolveResult(err, results) {
      var result = {};

      if (err) {
        return res.serverError(err);
      }

      if (results.length > 0) {
        result = results[0];
      }

      return res.ok(result);
    });
  }
})();
