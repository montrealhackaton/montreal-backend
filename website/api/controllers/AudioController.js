(function() {
  'use strict';

  var watson = require('watson-developer-cloud'),
      stp = require('stream-to-promise'),
      fs = require('fs'),
      fs = require('fs-extra'),
      q = require('q'),
      text_to_speech;

  module.exports = {
    buscarAudio: buscarAudio
  };

  iniciar();

  function executarQuery(query) {
    var  modelo = sails.models.produto;

    return q.ninvoke(modelo, 'query', query);
  }

  function iniciar() {
    text_to_speech = watson.text_to_speech({
      username: '982fc9ca-dbd6-49db-8859-4902a70c01b5',
      password: '3rt7QTwJ7r04',
      version: 'v1'
    });
  }

  function buscarAudio(requisicao, resposta) {
    var idProduto,
        pasta,
        caminhoCompleto,
        nome,
        atributo;

    idProduto = requisicao.params.id;
    atributo = requisicao.params.atributo;
    pasta = './recursos/audio/' + atributo;

    // Cria a pasta caso não exista ainda
    if (!fs.existsSync(pasta)){
      fs.mkdirsSync(pasta);
    }

    nome = idProduto + '.ogg';
    caminhoCompleto = pasta + '/' + nome;
    buscarAudioWatson(idProduto, atributo, caminhoCompleto).then(function(stream) {
      resposta.writeHead(200, {
        'Content-Type': 'audio/ogg'
      });
      //resposta.attachment(nome);
      resposta.end(fs.readFileSync(caminhoCompleto));
    }).catch(function(erro) {
      resposta.status(500).end(erro.stack);
    });
  }

  function buscarAudioWatson(idProduto, atributo, caminho) {
    if (fs.existsSync(caminho)){
      return q.fcall(function() {});
    }

    return buscarProduto(idProduto).then(function(resultado) {
      return sintetizar(resultado[0][atributo], caminho);
    }).catch(function(erro) {
      console.error(erro.stack);
    });
  }

  function sintetizar(texto, caminho) {
    var parametros,
        resultado;

    parametros = {
      text: texto,
      voice: 'pt-BR_IsabelaVoice', // Optional voice
      accept: 'audio/ogg'
    };

    // Pipe the synthesized text to a file
    resultado = text_to_speech.synthesize(parametros).pipe(fs.createWriteStream(caminho));

    return stp(resultado);
  }

  function buscarProduto(idProduto) {
    var query;

    query = "exec tool.find_one @schema = 'vitrine'," +
      "@table = 'produto'," +
      "@id = '" + idProduto + "';";

    return executarQuery(query);
  }
})();
